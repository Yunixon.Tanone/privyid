<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return [
        'lumen_version' => $router->app->version(),
        'app_version' => 'V1.0.0'
    ];
});

$router->group(['prefix' => 'lottery'], function () use ($router) {
    $router->get('/get_results', 'LotteryController@getResults');
    $router->get('/get_results_xtrsyz', 'LotteryController@getResultsXtrsyz');

    $router->get('/test_notif', 'LotteryController@testNotif');
});





