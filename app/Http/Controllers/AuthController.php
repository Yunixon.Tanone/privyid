<?php

namespace App\Http\Controllers;

use App\Models\LoginLog;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Auth\Events\Login;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use phpDocumentor\Reflection\Types\This;

class AuthController extends Controller
{
    const TOKEN_ACTIVE = '1';
    const TOKEN_NON_ACTIVE = '0';

    public function __construct()
    {
        $this->middleware('auth', ['except' => ['login']]);
    }

    public function login(Request $request)
    {
        $user = User::where([
            'username' => $request->username,
            'password' => $request->password
        ])
        ->select([
            'id','username','email'
        ])
        ->first();
        
        if (!$user) {
            return $this->errorWithMessage('Data Login Tidak Valid!');
        }

        LoginLog::where(['userId' => $user->id, 'is_active' => 1])->update(
            ['is_active' => SELF::TOKEN_NON_ACTIVE]
        ); // Logout Token Aktif yg Lain!

        $generatedToken = Hash::make($user->id.Carbon::now());
        LoginLog::create([
            'userId' => $user->id,
            'token' => $generatedToken
        ]);

        $user->token = $generatedToken;
        
        return $this->successWithData($user);
    }

    public function logout(Request $request)
    {
        $token = Auth::user()->token;
        LoginLog::where(['token' => $token])
            ->update(['is_active' => SELF::TOKEN_NON_ACTIVE]);

        return $this->successWithMessage('Berhasil Logout!');
    }
}
