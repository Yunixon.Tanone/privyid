<?php

namespace App\Http\Controllers;

use App\Events\LotteryEvent;
use App\Models\Subscriber;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

// use GuzzleHttp\Client;


class LotteryController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth', ['except' => []]);
    }
    
    function getResults(Request $request){
        //   $client = new Client(); // Documentation : http://docs.guzzlephp.org/en/stable/
        
        $games_id = ["9","10","11","12","13","14", "15", "16", "17", "18"];
        
        
        $client = new Client();
        
        foreach ($games_id as $game_id) {
            
            
            $payload = [
                'Game' => $game_id
            ];
            
            $res = $client->request('POST', 'http://54.64.105.174/DataNomor.aspx/GetPastResult', [
                'headers' => [
                    'Content-Type' => 'application/json'
                ],
                'json' => $payload
                ]);
                
                $response = json_decode($res->getBody()->getContents());
                
                $data = $response->d; 
                $rows = json_decode($data)->rows;
                
                $inserted_row = 0;
                foreach($rows as  $result) {
                    
                    
                    if($inserted_row >= 2){
                        break;
                    }
                    
                    $inserted_row++;
                    
                    try {
                        
                        DB::table('results')->insert([
                            'game_type_id' => $game_id,
                            'game_name' => $result->GameName,
                            'running_number' => $result->RunningNum,
                            'shio' =>  $result->Shio,
                            'result' =>  $result->Result,
                            'date' => Carbon::create($result->ResultDate) 
                            ]);

                            $country = $result->GameName;
                            
                                    $data = json_encode( [
                                        'title' => 'Hasil Togel Keluar',
                                        'body' => "Hallo Gan. Hasil Togel $country Yang Anda Ikuti Sudah Keluar!"
                                    ]);

                                    $subscribers = Subscriber::where(['game_namme' => $country])->get();

                                    foreach ($subscribers as $key => $subscriber) {
                                        event( new LotteryEvent($data, 'new_result', $subscriber->imei) );
                                    }



                        }
                        catch (\Throwable $th) {
                            continue;
                        }
                        
                    }
                }
                
                
                return $this->successWithMessage('Berhasil Ambil Data Kak!');
            }
            
            function getResultsXtrsyz(Request $request,HelperController $helperController){
                $client = new Client();
                $res = $client->request('GET', 'http://hasiltogel.xtrsyz.com/menu.php?name=tgToday', [
                    'headers' => [
                        'Content-Type' => 'application/json'
                    ],
                    // 'json' => $payload
                    ]);
                    
                    $response = json_decode($res->getBody()->getContents());
                    
                    $results = $response->menu;
                    $result_length = count($results) - 1;
                    
                    
                    // Remove First And Last Result Karna Tidak Valid!
                    unset($results[0]);
                    unset($results[$result_length]);
                    
                    $real_results = [];
                    
                    foreach($results as $result){
                        
                        $country = substr($result->title,0,-7);
                        $date =  Carbon::create(str_replace('Tanggal: ','', $result->desc))->toDateString();
                        $result = substr($result->title,-5,-1);
                        
                        if($country == 'Hongkong'){
                            $country = 'Hong Kong';
                        }
                        
                        $real_results[] = [
                            'country' => $country,
                            'date' => $date,
                            'result' => $result
                        ];
                        
                        $game_id = $helperController->setGameId($country);
                        
                        $existing_data = DB::table('results')->where([
                            'game_name' => $country,
                            'date' => $date
                            ])->first();
                            
                            if($existing_data){
                                continue;
                            }
                            
                            try {
                                DB::table('results')->insert([
                                    'game_name' => $country,
                                    'result' => $result,
                                    'date' => $date,
                                    'shio' => '-'
                                    ]);

                                    $data = json_encode( [
                                        'title' => 'Hasil Togel Keluar',
                                        'body' => "Hallo Gan. Hasil Togel $country Yang Anda Ikuti Sudah Keluar!"
                                    ]);

                                    $subscribers = Subscriber::where(['game_namme' => $country])->get();

                                    foreach ($subscribers as $key => $subscriber) {
                                        event( new LotteryEvent($data, 'new_result', $subscriber->imei) );
                                    }
                                } catch (\Throwable $th) {
                                    continue;
                                    
                                }
                                
                                
                            }
                            
                            return $this->successWithMessage('Berhasil menambahkan Data!');
                        }
                        
                        function testNotif(){
                            $data = json_encode( [
                                'title' => 'Ini Title',
                                'body' => 'Ini Bdoy'
                            ]);
                            event(
                                new LotteryEvent($data,'new_result','0340a5ce-3368-417d-b76a-c5acbbc931c4')
                            );
                            return $this->successWithMessage('Success gan');
                        }
                        
                        
                        
                        
                    }
                    