<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    public function successWithMessage($message)
    {
        return response()->json([
            'code'  => 1,
            'error' => null,
            'message' => $message,
            'data'  => null,
        ], 200);
    }

    public function successWithData($data)
    {
        return response()->json([
            'code'  => 1,
            'error' => null,
            'message' => 'Berhasil Ambil Data!',
            'data'  => $data,
        ], 200);
    }

    public function errorWithMessage($message)
    {
        return response()->json([
            'code'  => 0,
            'error' => null,
            'message' => $message,
            'data'  => null,
        ], 200);
    }

    public function errorWithData($data)
    {
        return response()->json([
            'code'  => 0,
            'error' => null,
            'message' => 'Gagal Mengambil Data!',
            'data'  => $data,
        ], 200);
    }
}
