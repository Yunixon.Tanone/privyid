<?php

namespace App\Http\Middleware;

use App\Models\LoginLog;
use Closure;
use Illuminate\Contracts\Auth\Factory as Auth;

class Authenticate
{
    /**
     * The authentication guard factory instance.
     *
     * @var \Illuminate\Contracts\Auth\Factory
     */
    protected $auth;

    /**
     * Create a new middleware instance.
     *
     * @param  \Illuminate\Contracts\Auth\Factory  $auth
     * @return void
     */
    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $apiToken = explode(' ', $request->header('Authorization'))[1] ?? "";

        $isTokenExist = LoginLog::where(['token' => $apiToken, 'is_active' => 1])->first();

        if (!$isTokenExist) {
            return response()->json([
            'code'  => 0,
            'error' => null,
            'message' => 'Token Tidak Valid. Silahkan Login Kembali Untuk Mendapatkan Token Baru!',
            'data'  => null,
        ], 200);
        }
        
        return $next($request);
    }
}
