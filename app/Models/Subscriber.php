<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subscriber extends Model
{
    protected $table = "subscribers";
    protected $fillable = [
        'id',
        'imei',
        'game_name',
        'created_at',
        'updated_at'
    ];
    protected $hidden = [];
}
