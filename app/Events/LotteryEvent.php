<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class LotteryEvent implements ShouldBroadcast
{
     use InteractsWithSockets, SerializesModels;

    public $message;
    public $channel;
    public $event;

    public function __construct($message = "", $channel = null, $event = null)
    {
        $this->message = $message;
        $this->channel = $channel;
        $this->event = $event;
    }

    public function broadcastOn()
    {
        return $this->channel;
    }

    public function broadcastAs()
    {
        return $this->event;
    }
}