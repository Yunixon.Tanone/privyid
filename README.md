Cara Penggunaan :

- Clone repository
- Jalankan command "composer install"
- Export database dari file privyid.sql
- Export collection postman dari file privyid.postman_collection.json

- Buka postman kemudian gunakan API Login untuk generate token user yang baru.
- Klik kanan pada nama collection privyid kemudian pilih edit dan pilih tab variable.
- Update value token dengan hasil generate token login tadi dan juga ganti base URL sesuai alamat web server pengguna. 

- API siap dipakai.
