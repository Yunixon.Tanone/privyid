-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.10-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for privyid
CREATE DATABASE IF NOT EXISTS `privyid` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `privyid`;

-- Dumping structure for table privyid.bank_balance
CREATE TABLE IF NOT EXISTS `bank_balance` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `balance` bigint(20) NOT NULL DEFAULT 0,
  `balance_achieve` bigint(20) NOT NULL DEFAULT 0,
  `code` varchar(50) DEFAULT NULL,
  `enable` enum('1','0') DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Dumping data for table privyid.bank_balance: ~0 rows (approximately)
DELETE FROM `bank_balance`;
/*!40000 ALTER TABLE `bank_balance` DISABLE KEYS */;
/*!40000 ALTER TABLE `bank_balance` ENABLE KEYS */;

-- Dumping structure for table privyid.bank_balance_history
CREATE TABLE IF NOT EXISTS `bank_balance_history` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `bankBalanceId` bigint(20) NOT NULL DEFAULT 0,
  `balanceBefore` bigint(20) NOT NULL DEFAULT 0,
  `balanceAfter` bigint(20) NOT NULL DEFAULT 0,
  `activity` varchar(255) DEFAULT NULL,
  `type` enum('DEBIT','CREDIT') DEFAULT NULL,
  `ip` varchar(50) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `userAgent` varchar(255) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Dumping data for table privyid.bank_balance_history: ~0 rows (approximately)
DELETE FROM `bank_balance_history`;
/*!40000 ALTER TABLE `bank_balance_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `bank_balance_history` ENABLE KEYS */;

-- Dumping structure for table privyid.login_logs
CREATE TABLE IF NOT EXISTS `login_logs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userId` bigint(20) NOT NULL,
  `token` varchar(255) NOT NULL,
  `is_active` enum('1','0') NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `FK_login_logs_user` (`userId`),
  CONSTRAINT `FK_login_logs_user` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table privyid.login_logs: ~12 rows (approximately)
DELETE FROM `login_logs`;
/*!40000 ALTER TABLE `login_logs` DISABLE KEYS */;
INSERT INTO `login_logs` (`id`, `userId`, `token`, `is_active`, `created_at`, `updated_at`) VALUES
	(1, 1, '$2y$10$HMzjsgKpr4l8XB/q6ZZN4OX6lwrffTNtdEgXrZUQ5voLmiSvIGphG', '0', '2021-02-10 03:34:41', '2021-02-10 04:18:22'),
	(2, 1, '$2y$10$/9SU4BPUwVhCGkvQ2wBTEuQ9wlHTw3cBEOiIOCa0UVCzkF4pVi.lu', '0', '2021-02-10 03:49:03', '2021-02-10 04:05:10'),
	(3, 1, '$2y$10$ADAknYvDN3.JI8GCWk1Pue/nXyW5HgjozSjIZUb0ER5oMljX7Mb4S', '0', '2021-02-10 03:53:13', '2021-02-10 04:18:22'),
	(4, 1, '$2y$10$er/kYILK6s4BL3SBWaSQlezPz998UQTMHb0iIK6ZpEABfyde2y/mW', '0', '2021-02-10 04:18:22', '2021-02-10 04:18:27'),
	(5, 1, '$2y$10$WUHukTNlDX7uf7zFhrc4S.TVe6KHRHgUxHlYCvRuLsRR66.0kBALm', '0', '2021-02-10 04:18:27', '2021-02-10 04:22:58'),
	(6, 1, '$2y$10$a2y3QmMeZHswBUo2UIuT2OIRBfb.eah8Nr3k77s/zdncDDQBX90K.', '0', '2021-02-10 04:22:58', '2021-02-10 04:23:01'),
	(7, 1, '$2y$10$B3L7txG.VfRETWwNZPLuKOFR41ULyEhMqC9eh5i/cNW6c7/j6yTKa', '0', '2021-02-10 04:23:01', '2021-02-10 04:23:03'),
	(8, 1, '$2y$10$5OO7t9pbjzk9tVkAKmIf2u/rLCWnI0bcswBiJHJZXGY1vL36rXwJi', '0', '2021-02-10 04:23:03', '2021-02-10 04:24:42'),
	(9, 1, '$2y$10$qOfZxvh4V4h38FL2Q3IqxOvPMH6Z83xZoiR8j7qpqsN7WHmH1pJxu', '0', '2021-02-10 04:24:42', '2021-02-10 04:24:57'),
	(10, 1, '$2y$10$bS54XV/dCm/BXwuoGq0vseOVygg38Aa8ETPOiitQiA4oUlX.XiQrK', '0', '2021-02-10 04:24:57', '2021-02-10 04:25:00'),
	(11, 1, '$2y$10$Ev0oj74Fwth65L/sFDKr0.qK7NEER680Rpw/RgOt7XAgZpZpBJXfe', '0', '2021-02-10 04:25:00', '2021-02-10 22:12:20'),
	(12, 1, '$2y$10$clhLrU/4xKBnq96f0NTmbu3v3nO1wSa1JS3KEnaSSLwQeCp/Ou2qS', '1', '2021-02-10 22:12:20', '2021-02-10 22:12:20');
/*!40000 ALTER TABLE `login_logs` ENABLE KEYS */;

-- Dumping structure for table privyid.user
CREATE TABLE IF NOT EXISTS `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table privyid.user: ~2 rows (approximately)
DELETE FROM `user`;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `username`, `email`, `password`, `created_at`, `updated_at`) VALUES
	(1, 'yunixon', NULL, '123', '2021-02-10 03:34:36', '2021-02-10 03:34:36'),
	(2, 'penerima', NULL, '232', '2021-02-11 06:38:42', '2021-02-11 06:38:44');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

-- Dumping structure for table privyid.user_balance
CREATE TABLE IF NOT EXISTS `user_balance` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userId` bigint(20) NOT NULL,
  `balance` varchar(50) DEFAULT NULL,
  `balanceAchieve` bigint(20) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `FK_user_balance_user` (`userId`),
  CONSTRAINT `FK_user_balance_user` FOREIGN KEY (`userId`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table privyid.user_balance: ~2 rows (approximately)
DELETE FROM `user_balance`;
/*!40000 ALTER TABLE `user_balance` DISABLE KEYS */;
INSERT INTO `user_balance` (`id`, `userId`, `balance`, `balanceAchieve`, `created_at`, `updated_at`) VALUES
	(4, 1, '2', 197, '2021-02-10 22:34:37', '2021-02-11 06:44:46'),
	(6, 2, '295', 295, '2021-02-11 06:38:51', '2021-02-11 06:44:45');
/*!40000 ALTER TABLE `user_balance` ENABLE KEYS */;

-- Dumping structure for table privyid.user_balance_history
CREATE TABLE IF NOT EXISTS `user_balance_history` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userBalanceId` bigint(20) NOT NULL,
  `balanceBefore` bigint(20) NOT NULL,
  `balanceAfter` bigint(20) NOT NULL,
  `activity` varchar(100) DEFAULT NULL,
  `type` enum('DEBIT','CREDIT') NOT NULL,
  `ip` varchar(50) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `userAgent` varchar(255) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `FK_user_balance_history_user_balance` (`userBalanceId`),
  CONSTRAINT `FK_user_balance_history_user_balance` FOREIGN KEY (`userBalanceId`) REFERENCES `user_balance` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table privyid.user_balance_history: ~20 rows (approximately)
DELETE FROM `user_balance_history`;
/*!40000 ALTER TABLE `user_balance_history` DISABLE KEYS */;
INSERT INTO `user_balance_history` (`id`, `userBalanceId`, `balanceBefore`, `balanceAfter`, `activity`, `type`, `ip`, `location`, `userAgent`, `author`, `created_at`, `updated_at`) VALUES
	(2, 4, 0, 99, 'INSERT', 'DEBIT', '127.0.0.1', '127.0.0.1', 'PostmanRuntime/7.26.8', '127.0.0.1', '2021-02-10 22:34:37', '2021-02-10 22:34:37'),
	(3, 4, 99, 198, 'UPDATE', 'DEBIT', '127.0.0.1', '127.0.0.1', 'PostmanRuntime/7.26.8', '127.0.0.1', '2021-02-10 22:36:21', '2021-02-10 22:36:21'),
	(4, 4, 198, 297, 'INSERT', 'DEBIT', '127.0.0.1', '127.0.0.1', 'PostmanRuntime/7.26.8', '127.0.0.1', '2021-02-10 22:37:57', '2021-02-10 22:37:57'),
	(5, 4, 297, 396, 'INSERT', 'DEBIT', '127.0.0.1', '127.0.0.1', 'PostmanRuntime/7.26.8', '127.0.0.1', '2021-02-10 22:47:44', '2021-02-10 22:47:44'),
	(6, 4, 396, 495, 'INSERT', 'DEBIT', '127.0.0.1', '127.0.0.1', 'PostmanRuntime/7.26.8', '127.0.0.1', '2021-02-11 06:35:25', '2021-02-11 06:35:25'),
	(7, 6, 0, 99, 'INSERT', 'DEBIT', '127.0.0.1', '127.0.0.1', 'PostmanRuntime/7.26.8', '127.0.0.1', '2021-02-11 06:38:52', '2021-02-11 06:38:52'),
	(8, 6, 99, 198, 'INSERT', 'DEBIT', '127.0.0.1', '127.0.0.1', 'PostmanRuntime/7.26.8', '127.0.0.1', '2021-02-11 06:39:19', '2021-02-11 06:39:19'),
	(9, 4, 495, 396, 'TRANSFER', 'CREDIT', '127.0.0.1', '127.0.0.1', 'PostmanRuntime/7.26.8', '127.0.0.1', '2021-02-11 06:39:19', '2021-02-11 06:39:19'),
	(10, 6, 99, 198, 'INSERT', 'DEBIT', '127.0.0.1', '127.0.0.1', 'PostmanRuntime/7.26.8', '127.0.0.1', '2021-02-11 06:39:42', '2021-02-11 06:39:42'),
	(11, 4, 198, 99, 'TRANSFER', 'CREDIT', '127.0.0.1', '127.0.0.1', 'PostmanRuntime/7.26.8', '127.0.0.1', '2021-02-11 06:39:42', '2021-02-11 06:39:42'),
	(12, 6, 99, 198, 'INSERT', 'DEBIT', '127.0.0.1', '127.0.0.1', 'PostmanRuntime/7.26.8', '127.0.0.1', '2021-02-11 06:41:54', '2021-02-11 06:41:54'),
	(13, 4, 99, 0, 'TRANSFER', 'CREDIT', '127.0.0.1', '127.0.0.1', 'PostmanRuntime/7.26.8', '127.0.0.1', '2021-02-11 06:41:54', '2021-02-11 06:41:54'),
	(14, 6, 99, 197, 'INSERT', 'DEBIT', '127.0.0.1', '127.0.0.1', 'PostmanRuntime/7.26.8', '127.0.0.1', '2021-02-11 06:42:20', '2021-02-11 06:42:20'),
	(15, 4, 1, -97, 'TRANSFER', 'CREDIT', '127.0.0.1', '127.0.0.1', 'PostmanRuntime/7.26.8', '127.0.0.1', '2021-02-11 06:42:20', '2021-02-11 06:42:20'),
	(16, 6, 99, 197, 'INSERT', 'DEBIT', '127.0.0.1', '127.0.0.1', 'PostmanRuntime/7.26.8', '127.0.0.1', '2021-02-11 06:43:40', '2021-02-11 06:43:40'),
	(17, 4, 2, -96, 'TRANSFER', 'CREDIT', '127.0.0.1', '127.0.0.1', 'PostmanRuntime/7.26.8', '127.0.0.1', '2021-02-11 06:43:40', '2021-02-11 06:43:40'),
	(18, 6, 99, 197, 'INSERT', 'DEBIT', '127.0.0.1', '127.0.0.1', 'PostmanRuntime/7.26.8', '127.0.0.1', '2021-02-11 06:44:33', '2021-02-11 06:44:33'),
	(19, 4, 2, -96, 'TRANSFER', 'CREDIT', '127.0.0.1', '127.0.0.1', 'PostmanRuntime/7.26.8', '127.0.0.1', '2021-02-11 06:44:33', '2021-02-11 06:44:33'),
	(20, 6, 197, 295, 'INSERT', 'DEBIT', '127.0.0.1', '127.0.0.1', 'PostmanRuntime/7.26.8', '127.0.0.1', '2021-02-11 06:44:46', '2021-02-11 06:44:46'),
	(21, 4, 2, -96, 'TRANSFER', 'CREDIT', '127.0.0.1', '127.0.0.1', 'PostmanRuntime/7.26.8', '127.0.0.1', '2021-02-11 06:44:46', '2021-02-11 06:44:46');
/*!40000 ALTER TABLE `user_balance_history` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
